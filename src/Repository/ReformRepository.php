<?php 

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;

class ReformRepository
{

	public function __construct(EntityManagerInterface $em) {
		$this->em = $em;
	}

	public function getReforms($limit, $sector = 0)
	{
		$sql = 'SELECT * FROM reform WHERE 1';

		if ($sector) {
			$sql .= ' AND reform_sector = :sector';
		}

		$sql .= ' LIMIT ' . (int)$limit;

		$statement = $this->em->getConnection()->prepare($sql);

		if ($sector) {
			$statement->bindValue('sector', (int)$sector);
		}

		$result = $statement->execute()->fetchAll();

		return $result;
	}

	public function getReform($id)
	{
		$sql = 'SELECT * FROM reform WHERE reform_id = :id LIMIT 1';

		$statement = $this->em->getConnection()->prepare($sql);
		$statement->bindValue('id', (int)$id);

		$result = $statement->execute()->fetch();

		return $result;
	}

}
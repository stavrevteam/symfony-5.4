<?php

namespace App\Utils;

//~ use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Repository\ReformRepository;

class Reform
{

	public function __construct(/*ContainerInterface $container, */ReformRepository $repo)
	{
		//~ $this->container = $container;
		$this->repo = $repo;
	}

	public function getReforms($limit)
	{
		$items = $this->repo->getReforms($limit, 0);

		return $items;
	}

	public function getReform($id)
	{
		$item = $this->repo->getReform($id);

		return $item;
	}

}

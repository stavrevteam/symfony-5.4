<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//~ use Symfony\Component\HttpFoundation\Request;
//~ use Symfony\Component\HttpFoundation\Session\SessionInterface;

use App\Utils\Reform;

class HomepageController extends AbstractController
{

	public function __construct(ContainerInterface $container, Reform $util)
	{
		$this->util = $util;
		$this->config = $container->getParameter('homepage');
	}

	/**
	 * @Route("/", name="homepage")
	 */
	public function index()
	{
		$reforms = $this->util->getReforms($this->config['limit']);

		$data = [
			'reforms' => $reforms,
		];

		return $this->render('homepage.html.twig', $data);
	}
}
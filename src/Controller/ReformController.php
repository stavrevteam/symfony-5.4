<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//~ use Symfony\Component\HttpFoundation\Request;
//~ use Symfony\Component\HttpFoundation\Session\SessionInterface;

use App\Utils\Reform;

class ReformController extends AbstractController
{

	public function __construct(ContainerInterface $container, Reform $util)
	{
		$this->util = $util;
	}

	/**
	 * @Route("/reform/{id}/", name="reform", requirements={"id": "\d+"})
	 */
	public function reformAction($id)
	{
		$reform = $this->util->getReform($id);

		$data = [
			'reform' => $reform,
		];

		return $this->render('reform.html.twig', $data);
	}
}
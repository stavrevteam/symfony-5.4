# Symfony #

Symfony PHP Framework notes by Simeon Stavrev.

### Links ###

* Quick summary
* [Symfony Official website](https://symfony.com/)
* [Symfony Documentation](https://symfony.com/doc/current/index.html)

# Server Configuration

https://symfony.com/doc/current/setup.html

## 1. PHP modules

https://bitbucket.org/stavrevteam/debian-linux/src/master/LAMP.md

## 2. Server Symfony Installation

~~~
cd /root
wget https://get.symfony.com/cli/installer -O - | bash
mv /root/.symfony/bin/symfony /usr/local/bin/symfony
rm /root/.symfony/
symfony check:requirements
~~~

## 3. Server Composer Installation:

~~~
cd /root
https://getcomposer.org/download/ -> /root/composer.phar
chmod 755 composer.phar
mv composer.phar /usr/local/bin/composer
composer -V
~~~

---

# New Symfony Project Installation:

Without --full parameter will be a clean install without Twig and Profiler
~~~
symfony new symfony
symfony new symfony --full
php bin/console about
~~~

## Parameters

https://symfony.com/doc/current/configuration.html

@config/services.yaml -> parameters:

@Controller -> $container->getParameter('someparam')

## Controller

https://symfony.com/doc/current/controller.html


## Utils

* https://symfony.com/doc/4.2/best_practices/business-logic.html
* https://symfony.com/doc/current/service_container.html

## Repositories

https://symfony.com/doc/current/doctrine.html#querying-with-sql

No --full:
~~~
composer require symfony/orm-pack
~~~

## Twig

https://symfony.com/doc/current/templates.html

No --full:
~~~
composer require symfony/twig-bundle
~~~

## Profiler

https://symfony.com/doc/current/profiler.html

No --full:
~~~
composer require --dev symfony/profiler-pack
~~~

~~~
composer require symfony/apache-pack
~~~

## .env

https://symfony.com/doc/current/configuration.html

Create .env.local for DB credentials - gitignored. Set up:

~~~
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
~~~